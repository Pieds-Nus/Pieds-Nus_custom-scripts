#!/bin/bash

echo -en "_________________\n     CPU: "

tmp=$(echo "($(acpitool -c | head -n3 | tail -n 1 | rev | cut -d " " -f 2 | rev)-800)/26" | bc)

echo -e "$tmp%\t\t\t\t\t\t$(progressbar $tmp)"

tmp=$(echo " ( $(cat /sys/class/thermal/thermal_zone*/temp | tr "\n" "+") 0 ) / $( ls -d /sys/class/thermal/thermal_zone* | wc -l ) " | bc | rev | cut -c 4- | rev)
echo -e "\t  $tmp°C\t\t\t\t\t\t$(progressbar $tmp)"

echo -e "_________________\n     RAM: \n"

#tmp=($(free | tail -n 2 | cut -c 10-32))
tmp=($(free | tail -n 2 | tr -s " " |  cut -d " " -f 2,4,7))

h=$(free -h | cut -c 1-45)

echo "               total        used        free"



#free -h | grep Mem | cut -c 1-45
echo "$h" | cut -z -d "
" -f 2 

r=$(echo "(${tmp[0]}-${tmp[1]})*100/${tmp[0]}" | bc)
echo -ne "\t$r%\t"
progressbar  "$r"


echo "$h" | cut -z -d "
" -f 3

r=$(echo "(${tmp[3]}-${tmp[4]})*100/${tmp[3]}" | bc)
echo -ne "\t$r%\t"
progressbar  "$r"


echo -e "\n_________________\n     Swaps:\n"

s=$(swaps | sort)
p=($(echo "$s" | cut -d "	" -f 3 | tr -d "%"))
n=$(echo "$s" | wc -l)
i=1

while test $i -le $n ; do
	echo "$s" | cut -z -d "
" -f $i 
	echo -en "\t\t\t\t"
	progressbar  ${p[(($i-1))]}
	((i++))
done

echo -e "\n_________________\n     Disks:\n"
df=$(df -h --print-type | grep ext4 | \
					sed "s/\/dev\/sd[a|b]1              ext4/boot/" | \
					sed "s/\/dev\/mapper\/root_crypt ext4/root/" | \
					sed "s/\/dev\/mapper\/data_crypt ext4/data/" )

p=($(df | grep /dev/ | grep -v tmpfs | tr -s " " | cut -d " " -f 5 | tr -d "%"))

echo "$df" | cut -z -d "
" -f 1 
echo -en "\t\t\t"
progressbar  ${p[0]}

echo "$df" | cut -z -d "
" -f 2 
echo -en "\t\t\t"
progressbar  ${p[1]}

echo "$df" | cut -z -d "
" -f 3
echo -en "\t\t\t"
progressbar  ${p[2]}


test "$1" = "hold" && read -sn 1 aaa





