#!/bin/bash

# Dependencies
#acpitools
#nmcli
#awk
#bc 

remode()	{
	echo $((($(tail -n 1 /tmp/pnstatus_mode) $1 1 + 3 ) % 3)) >> /tmp/pnstatus_mode
	kill $waitpid
}

trap "remode +" SIGUSR1
trap "remode -" SIGUSR2

echo -e "\
Mode control for pnstatus script\n\
Write the desired mode as last line of this file\n\
\t0|min :\tminimal mode :\t| 23:25\n\
\t1|some :\tintermediate mode :\t| Wifi | CPU:RAM:SWAP | BATTERY | DA/TE TI:ME\n\
\t1|all :\t complete mode :\t| Network | CPU:RAM:SWAP | TEMP | BATTERY | DA/TE TI:ME\n\
0" \
> /tmp/pnstatus_mode

command=echo
if test "$1" = "-x" ; then
	# echo "-x"
	command="xsetroot -name"
	shift 1
fi
if test "$1" = "-c" ; then
	# echo "-c"
	command="$2"
	shift 2
fi
# echo $command

echo ${1:-0} >> /tmp/pnstatus_mode

while true ; do
	res=""
	case $(tail -n 1 /tmp/pnstatus_mode) in
		0|"min")
			res="$(date '+%H:%M')"
			sleep 30 & waitpid=$!
		;;
	
		1|"some")
			
			connect=""
			#Check ethernet interface status
			grep "up" /sys/class/net/enp0s25/operstate > /dev/null && connect=$connect\E
			#Check wifi interface status
			grep "up" /sys/class/net/wlp3s0/operstate > /dev/null && connect=$connect\W

		#	CPU load (divided by 4 cause 4-core i5 cpu. 
		#	Dirty way of dealing with bash and non-integer calculations : )
		#	| tr -d "." removes the decimal point read in /proc/loadavg
		#	If it is always written with three significative numbers, should work...
			cpu="$(echo "($(acpitool -c | head -n 3 | tail -n 1 | rev | cut -d " " -f 2 | rev)-800)/26" | bc)"

		#	ram usage	
			ram=$(free | head -n 2 | tail  -n 1 | awk '{printf( "%.0f", $3/$2*100 )}')

		#	swap usage
			swap=$(head -n 16 /proc/meminfo | tail -n 2 | tr -s " " | cut -d " " -f 2 | tr "\n" " " | awk '{printf( "%.0f", ($1-$2)*100/$1)}')
			
		#	battery state
			bat=$(cat /sys/class/power_supply/BAT1/capacity)
			case "$(cat /sys/class/power_supply/BAT1/status)" in 
				"Full") batstate="" ;;
				"Discharging") batstate="-" ;;
				"Charging") batstate="+" ;;
				"Not charging") batstate="." ;;
				*) batstate="?" ;;
			esac
			
			res="| $connect | $cpu:$ram:$swap | B:$batstate$bat% | $(date '+%a %d/%m %H:%M')"
			
			sleep 2 & waitpid=$!
			;;

		2|"all")
		#	Network
	#Should look into iw dev wls3p0 scan dump to get that info quicker
			wifi=$(timeout 0.5 nmcli -f IN-USE,SSID,BARS d wifi list | grep ^\* | cut -c 9- | tr -s " ")
			grep -q "up" /sys/class/net/enp0s25/operstate && eth=eth
			test -n "$wifi" && test -n "$eth" && eth=" - $eth"
			res=$res"| $wifi$eth"

		#CPU
		#	cpu1=$(cut -d " " -f 1 /proc/loadavg)
		#	cpu2=$(cut -d " " -f 2 /proc/loadavg)
		#	cpu=$(awk '{printf( "%.0f", $cpu1*25 "% - " $cpu2*25 "%" )}')
			
			#cpu=$(cut -d " " -f 1,2 /proc/loadavg | awk '{print $1*25 "%-" $2*25 "%" }')

			#cpu freq
			res=$res" | C:$(echo "($(acpitool -c | head -n3 | tail -n 1 | rev | cut -d " " -f 2 | rev)-800)/26" | bc)"

			#cpu load avg (1 minute) (4 thread-processor)
			res=$res"  L:$(echo "scale=0 ; $(cut -d " " -f 1 /proc/loadavg) * 25/1" | bc)"

		#RAM
			mems=($(head -n 3 /proc/meminfo | grep -v MemFree | tr -s " " | cut -d " " -f 2))
			res="$res | R:$(((${mems[0]}-${mems[1]})*100/${mems[0]}))"


		#SWAP
			res="$res S:$(head -n 16 /proc/meminfo | tail -n 2 | tr -s " " | cut -d " " -f 2 | tr "\n" " " | awk '{printf( "%.0f", ($1-$2)*100/$1)}')"



		#TEMP °C
		#Generic way :
		#	$(( ( $(cat /sys/class/thermal/thermal_zone*/temp | rev | cut -c 4- | rev | tr "\n" "+") 0 ) / $(ls -d /sys/class/thermal/thermal_zone* | wc -w) ))
		#Specific for only 2 thermal_zones :
			res="$res | T:$(( ( $(cat /sys/class/thermal/thermal_zone0/temp | rev | cut -c 4- | rev) +  $(cat /sys/class/thermal/thermal_zone1/temp | rev | cut -c 4- | rev) ) / 2 ))°C"

		#BATTERY
					bat=$(cat /sys/class/power_supply/BAT1/capacity)
					batstate=$(cat /sys/class/power_supply/BAT1/status)
					case "$batstate" in 
						"Full") batstate="" ;;
						"Discharging") batstate="-" ;;
						"Charging") batstate="+" ;;
						"Not charging") batstate="." ;;
						*) batstate="?" ;;
					esac
			res="$res | B:$bat%$batstate"

		#DATE
			res="$res | $(date "+%a %d/%m %H:%M")"
			
			sleep 2 & waitpid=$!
			;;
		*)
			echo $usage
			exit 1
			;;
	esac
	$command "$res"
	wait $waitpid
done

