#!/bin/bash

usage="usage: $0 { -cw|-cc } \nRotate the screen clockwise|counter-clockwise using xrandr."

if test ! $# -eq 1 ; then
	echo -e $usage
	echo "Invalid number of argument"
	exit 1
elif test "$1" != "-cw" -a "$1" != "-cc"  ; then
	echo -e $usage
	echo "Unrecognized option : $1"
	exit 1
fi

cur=$(xrandr -q | grep "eDP-1" | cut -d '(' -f 1 | rev | cut -d ' ' -f 2 | rev)

case "$cur" in 
	"left")
		test $1 = "-cw" && xrandr -o 0 && echo 1
		test $1 = "-cc" && xrandr -o 2 && echo 2
		;;
	"right")
		test $1 = "-cw" && xrandr -o 2 && echo 3
		test $1 = "-cc" && xrandr -o 0 && echo 4
		;;
	"inverted")
		test $1 = "-cw" && xrandr -o 1 && echo 5
		test $1 = "-cc" && xrandr -o 3 && echo 6
		;;
	*)
		test $1 = "-cw" && xrandr -o 3 && echo 7
		test $1 = "-cc" && xrandr -o 1 && echo 8
		;;
esac
exit $?
