#!/bin/bash

while read line ; do 

	echo "$line"| grep "Filename" > /dev/null && continue
	
	name=$(echo $line | cut -d " " -f 1)
	total=$(echo $line | cut -d " " -f 3)
	used=$(echo $line | cut -d " " -f 4)
	perc=$((100*used/total))
	
	echo -e "$name\t($(($total/1024000))GB)\t$perc%" | sort
done < /proc/swaps 
