#/bin/bash
usage="\
$0 up|down|min|max|set
Simple binder between screen brightness, hotkeys (via sxhkd) and herbe notifications
"

test $# -eq 0 -o $# -gt 2 && echo "$usage" && "One or two arguments expected" && exit 1 ;

#Run brightnessctl without arguments to get this value (statically stored to avoid multiple calls)
#max=851
#Minimal desired brightness, set to your taste. If 0, the screen will actually blank, set higher if you prefer.
#min=1

case $1 in
	
	"down")
		#lvl=`brightnessctl set ${2:-3}%+ | grep Current | cut -d '(' -f 2 | tr -d '%)'`
		lvl=`brightnessctl get` #; echo $lvl ; 
#		let lvl=lvl*9/10 #; echo $lvl ; 
		
		if test $lvl -lt 50 ; then
			let lvl=lvl-5
		else
			let lvl=lvl*9/10
		fi
		brightnessctl -q set $lvl
		;;
	
	"up")
		#lvl=`brightnessctl set ${2:-3}%- | grep Current | cut -d '(' -f 2 | tr -d '%)'`
		lvl=`brightnessctl get` #; echo $lvl ; 
#		let lvl=lvl*10/9 #; echo $lvl ; 
		
		if test $lvl -lt 50 ; then
			let lvl=lvl+5
		else
			let lvl=lvl*10/9
		fi
		brightnessctl -q set $lvl
		;;
	
	"min")
		lvl=1
		brightnessctl -q set 1 | grep Current | cut -d '(' -f 2 | tr -d '%)'
		;;
	
	"max")
		lvl=851
		brightnessctl -q set 851 | grep Current | cut -d '(' -f 2 | tr -d '%)'
		;;
	
	"set")
		test $((2*=1)) -eq 0 && echo $usage && echo "Invalid argument: $2 (integer expected in the range 1-100)"
		lvl=`brightnessctl set $2%+ | grep Current | cut -d '(' -f 2 | tr -d '%)'`
		;;
	
	*)
		echo "$usage"
		echo "Unrecognized argument: $1"
		exit 1
		;;
esac

ls /tmp/dsblok.pid &> /dev/null && /home/karsaell/Downloads/dsblocks/sigdsblocks/sigdsblocks 2 $lvl


#echo lvl: $lvl

for i in {1..851..25} ; do
	if test $i -lt $lvl ; then
		herbe="$herbe*"
	else
		herbe="$herbe "
	fi
done

test -s /tmp/herbe_brightness_pid && kill -s SIGUSR1 `cat /tmp/herbe_brightness_pid` && rm /tmp/herbe_brightness_pid

herbe "$herbe" &
pid=$!
echo $pid >> /tmp/herbe_brightness_pid

wait $pid

test -e /tmp/herbe_brightness_pid && echo -n `cat /tmp/herbe_brightness_pid | grep -v $pid` > /tmp/herbe_brightness_pid


