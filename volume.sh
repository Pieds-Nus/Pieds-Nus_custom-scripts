#/bin/bash
usage="\
$0 on|off|toggle|set|up|down [VALUE]
Simple binder between amixer, hotkeys and herbe notifications
"

test $# -eq 0 -o $# -gt 2 && echo -e "$usage\nOne or two arguments expected" && exit 1 ;


case $1 in
	"up")
		vol="$(amixer set Master ${2:-3}%+	| tail -n 1 | cut -d ' ' -f 7 | tr -d '[]%')" ;;
	"down")
		vol="$(amixer set Master ${2:-3}%-	| tail -n 1 | cut -d ' ' -f 7 | tr -d '[]%')" ;;
	"on")
		vol="$(amixer set Master on			| tail -n 1 | cut -d ' ' -f 7 | tr -d '[]%')" ;;
	"off")
		vol="$(amixer set Master off		| tail -n 1 | cut -d ' ' -f 7 | tr -d '[]%')" ;;
	"toggle")
		vol="$(amixer set Master toggle		| tail -n 1 | cut -d ' ' -f 7 | tr -d '[]%')" ;;
	"set")
		vol=$2
		amixer -q set Master "$vol%" > /dev/null ;;
	*)
		echo "$usage"
		echo "Unrecognized argument: $1"
		exit 1 ;;
esac

ls /tmp/dsblok.pid &> /dev/null && /home/karsaell/Downloads/dsblocks/sigdsblocks/sigdsblocks 1 $vol

#echo vol: $vol

for i in {1..100..3} ; do
	if test $i -lt $vol ; then
		herbe="$herbe|"
	else
		herbe="$herbe "
	fi
done

test -s /tmp/herbe_volume_pid && ( kill -s SIGUSR1 $(cat /tmp/herbe_volume_pid) ; rm /tmp/herbe_volume_pid )

amixer get Master | grep -q "%] \[on\]" && state=on || state=off

case $state in
	on)
		herbe.norm " $herbe" &
		pid=$!
		echo $pid >> /tmp/herbe_volume_pid
		;;
	off)
		herbe.low " $herbe" &
		pid=$!
		echo $pid >> /tmp/herbe_volume_pid
		;;
esac

wait $pid

test -e /tmp/herbe_volume_pid && echo -n `cat /tmp/herbe_volume_pid | grep -v $pid` > /tmp/herbe_volume_pid
