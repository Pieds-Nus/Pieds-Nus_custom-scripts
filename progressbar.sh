#!/bin/bash

usage() { echo "Usage:  $0 [-c bar|dot|o] [-s str] [-z] VALUE" 1>&2; ${1:+echo $1} ; exit 1; }

while getopts ":c:s:hz:" o; do
	case "${o}" in
		c)
			case ${OPTARG} in 
				"bar") replace="" ;;
				"dot") replace=*\"\' ;;
				"o"|"O") replace="Oo.";;
				*) usage "-c option only accepts \"bar\", \"dot\" or \"o\"" ;;
			esac
			;;
		s)
			if test $(echo -n ${OPTARG} | wc -c ) -eq 3 ; then
				replace=${OPTARG}
			else
				usage "-s option requires a 3-character-long string"
			fi
			;;
		z)
			z=-n
			;;
		*)
            usage "Invalid argument: $o $OPTARG"
			;;
	esac
done

shift $((OPTIND-1))

test $# -ne 1 && usage

value=${value:-$1}

test $value -ge 0 -a $1 -le 100 || usage "VALUE must be an integer between 0 and 100"

for j in {3..100..3} ; do 
	if [[ j -gt $value ]] ; then
		case $(($value%3)) in
			0) res=$res"";;
			1) res=$res".";;
			2) res=$res":";;
		esac
		break
	fi
	res=$res"|"
done

res=$res"                                                                                                    "
res=[${res:1:32}]

test $replace && res=$(echo "$res" | tr "|:." "$replace")

echo $z "$res"
